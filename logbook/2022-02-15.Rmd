# 15 February 2022 {-}

**Number of hours**: 1 hours class + 2 hours tutorial

## What was the activity? {-}

We sorted the possible speakers to start sending invitations.

## What did I learn? {-}

N/A.

## How will this impact my future practice? {-}

N/A.

## What forms of evidence did I produce? {-}

Online document with the names of possible speakers.
