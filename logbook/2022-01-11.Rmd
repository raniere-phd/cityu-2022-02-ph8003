# 11 January 2022 {-}

**Number of hours**: 2 hours of class plus 1 hour of tutorial

## What was the activity? {-}

Today I attended the first lesson of PH8003.
During the lesson,
I answer the questions for the *Belbin test*.

## What did I learn? {-}

I learnt how my performance in the course will be evaluated.
Also,
I learnt about the nine *Belbin Team Roles*:

- Chairman,
- Shaper,
- Plant,
- Resource Investigator,
- Monitor-Evaluator,
- Company Worker,
- Team Worker, and
- Completer-Finisher.

## How will this impact my future practice? {-}

I will be more attentive to
typical features,
positive qualities,
and
allowable weakness
that my colleagues bring based in their *Belbin Team Roles*.

## What forms of evidence did I produce? {-}

Evidence of my participation in the class my *Belbin test*.
